# Muhasib
Muhasib is a cross-platform application for muslim prayer accounting. Muhasib is made using the kivy framework of python.
Although initially Muhasib was just for the accounting but it includes getting accurate prayer times and hijri calendar functionalities.

## Built With

* [Python](https://www.python.org/) - The language used
* [Kivy](https://kivy.org) - Framework for cross-platform NUI development
* [Plyer](https://kivy.org) - To get native android functionalities
* [Matplotlib](https://matplotlib.org/) - Used to create the graphs in the prayer record graphs screen
* [Matplotlib Kivy backend](https://github.com/kivy-garden/garden.matplotlib) - Used to include matplotlib graphs in kivy
* [Navigation Drawer](https://github.com/kivy-garden/garden.navigationdrawer) - Used to create the app navigation system

## Acknowledgments

* [Prayer Times](https://www.python.org/) - The prayer time calculation algorithm used
* [Jaro-winkler](https://pypi.org/project/jellyfish/) - The location search uses the jaro-winkler algorithm which was taken from the jellyfish library
